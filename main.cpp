#include <fstream>
#include <iostream>
#include <string>

#include "Trie.h"

using namespace std;

void printMenu(){
    cout << "DSA assignment" << endl;
    cout << endl;
    cout << "[1] Check spelling of word" << endl;
    cout << "[2] Check spelling of words in file" << endl;
    cout << "[3] Add a new word to dictionary" << endl;
    cout << "[4] Write dictionary to file" << endl;
    cout << "[5] Display words that starts with a specific letter" << endl;
    cout << "[6] Check spelling of word with errors" << endl;
    cout << "[0] Exit" << endl;
}

void wordCheck(Trie *dict){
    cout << "Enter word to be checked: ";
    string word;
    cin >> word;
    if(dict->find(word)){
        cout << "Word is spelled correctly\n" << endl;
    }
    else{
        cerr << "Word is not spelled correctly\n" << endl;
    }
}

void fileCheck(Trie *dict){
    cout << "Enter filename to be checked: ";
    string filename;
    cin >> filename;
    string word;
    ifstream file;
    file.open(filename);
    if(!file.is_open()){
        cerr << "Unable to open file!" << endl;
        return;
    }
    while(!file.eof()){
        getline(file,word);
        if(!dict->find(word)){
            // remove trailing empty char caused by getline
            if(word == ""){
                continue;
            }
            cout << word << " was spelled wrongly/not in dictionary" << endl;
        }
    }
}

void wordAdd(Trie *dict){
    cout << "Enter word to be added to the dictionary: ";
    string word;
    cin >> word;
    dict->push(word);
    cout << "Word successfully added" << endl;
    cout << "Dictionary must be saved for change to be permanent" << endl;
}

void dictWrite(Trie *dict){
    
    ofstream dictionary;
    dictionary.open("dictionary.txt");
    if(!dictionary.is_open()){
        cerr << "Error opening dictionary.txt for writing" << endl;
        return;
    }
    vector<string> words = dict->getWords();
    for(int i=0;i<words.size();i++){
        if(i == words.size()-1){ // if last word do not insert newline
            dictionary << words[i];
        }
        else{
            dictionary << words[i] << endl;
        }
    }
    dictionary.close();
}

void startWith(Trie *dict){
    cout << "Enter alphabet to search: ";
    char alphabet;
    cin >> alphabet;
    vector<string> words = dict->getWords(alphabet);
    cout << "===========================" << endl;
    for(int i=0; i<words.size();i++){
        cout << words[i] << endl;
    }
    cout << "===========================" << endl;
}

void errorCheck(Trie *dict){
    cout << "Enter word" << endl;
    string word;
    cin >> word;

    TrieResult results = dict->findWithError(word);
    if(results.addError && results.found){cout << "Word has additional character(s)" << endl;}
    else if(results.subError && results.found){cout << "Word is wrong character(s)" << endl;}
    else{cout << "Word is wrong but program is unable to tell why" << endl;}
}

int main(){
    Trie *dict = new Trie();
    // load words into trie
    ifstream dictionary;
    dictionary.open("dictionary.txt");
    if(!dictionary.is_open()){
        cerr << "Error opening dictionary.txt" << endl;
        return 0;
    }
    string word;
    while(!dictionary.eof()){
        getline(dictionary, word);
        dict->push(word);
    }
    dictionary.close();
    // options
    while (true){
        cout << endl;
        printMenu();
        cout << "\noption: ";
        char option;
        cin >> option; 
        switch (option)
        {
            case '1':
                wordCheck(dict);
                break;
            case '2':
                fileCheck(dict);
                break;
            case '3':
                wordAdd(dict);
                break;
            case '4':
                dictWrite(dict);
                break;
            case '5':
                startWith(dict);
                break;
            case '6':
                errorCheck(dict);
                break;
            case '0':
                return 0;
                break;
            default:
                cout << "No such option" << endl;
                break;
        }
    }
}